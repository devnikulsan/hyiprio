import React, { useState, useEffect, useRef } from 'react';
import { ChevronDown } from 'react-feather';

const DropdownNav = ({ menuItems }) => {
  const [isOpen, setIsOpen] = useState(false);
  const dropdownRef = useRef(null);

  const handleToggle = () => {
    setIsOpen(!isOpen);
  };

  const handleLogout = () => {
    // Implement your logout logic here
    console.log('Logged out');
  };

  useEffect(() => {
    const handleOutsideClick = (event) => {
      if (dropdownRef.current && !dropdownRef.current.contains(event.target)) {
        setIsOpen(false);
      }
    };

    document.addEventListener('mousedown', handleOutsideClick);

    return () => {
      document.removeEventListener('mousedown', handleOutsideClick);
    };
  }, []);

  return (
    <div className="relative inline-block" ref={dropdownRef}>
      <button
        onClick={handleToggle}
        className="text-black font-semibold py-2 px-4 rounded focus:outline-none focus:shadow-outline flex items-center cursor-pointer"
      >
        <span className="mr-2">User123</span>
        <ChevronDown size={16} />
      </button>
      {isOpen && (
        <div className="absolute right-0 mt-2 py-2 w-48 bg-white rounded-lg shadow-lg">
          {menuItems.map((item) => (
            <a
              key={item.id}
              href={item.link}
              className="block px-4 py-2 text-gray-800 hover:bg-blue-500 hover:text-white"
            >
              {item.label}
            </a>
          ))}
          <hr />
          <button
            onClick={handleLogout}
            className="w-full text-start block px-4 py-2 text-gray-800 hover:bg-blue-500 hover:text-white"
          >
            Logout
          </button>
        </div>
      )}
    </div>
  );
};

export default DropdownNav;
