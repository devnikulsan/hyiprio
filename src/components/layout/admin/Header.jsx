import React from 'react'
import DropdownNav from './DropdownNav'
import {
  CollapsIcon
} from "../../icons";
import { setToggleCollapseReducer } from "@/redux/features/sidebar/sidebar-slice";
import { useDispatch, useSelector } from "react-redux";
import classNames from "classnames";

export default function Header() {
  
  const dispatch = useDispatch()

  const toggleCollapseReducer = useSelector(state => state.sidebarReducer.toggleCollapse)

  const collapseIconClasses = classNames(
    "p-4 rounded bg-light-lighter absolute left-8 transition-all",
    {
      "rotate-180": toggleCollapseReducer,
    }
  );

  const handleSidebarToggle = () => {
    dispatch(setToggleCollapseReducer(!toggleCollapseReducer))
  };

  const menuItems = [
    { id: 1, label: 'My Profile', link: '#' },
    { id: 2, label: 'Downloads', link: '#' },
    // Add more menu items as needed
  ];

  return (
    <div className='py-4 mx-24 z-50'>
      <button
        className={collapseIconClasses}
        onClick={handleSidebarToggle}
      >
        <CollapsIcon fill="black"/>
      </button>
      <div className='flex justify-between'>
        <div className='flex justify-center items-center font-semibold'></div>
        <DropdownNav menuItems={menuItems} />
      </div>
    </div>
  )
}
