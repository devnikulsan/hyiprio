"use client"
import classNames from "classnames";
import Link from "next/link";
import { useRouter, usePathname } from "next/navigation";
import React, { useMemo, useState } from "react";
import {
  HomeIcon,
  LogoIcon,
  LogoutIcon,
  UsersGroup,
  ColumnsIcon,
  UsersIcon,
  EnvelopeIcon,
  MessageIcon,
  ChartIcon
} from "../../icons";
import { useSelector } from "react-redux";

const menuItems = [
  { id: 2, label: "", subItems: [
    { id: 'dashboard', label: "Dashboard", link: "/admin", icon: ChartIcon, },
  ] },
  { id: 2, label: "CUSTOMER MANAGEMENT", subItems: [
    { id: 'customers', label: "Customers", icon: ChartIcon, subItems: [
      { id: 'allCustomers', label: 'All Customers', link: "/admin/user" },
      { id: 'activeCustomers', label: 'Active Customers', link: "/admin/user/active" },
      { id: 'disabledCustomers', label: 'Disabled Customers', link: "/admin/user/inactive" },
      { id: 'notifications', label: 'Notifications', link: "/admin/notification" },
      { id: 'sendEmail', label: 'Send Email To All', link: "/admin/user/mail-send/all" }
    ] },
    { id: 'kyc_management', label: "KYC Management", icon: MessageIcon,  subItems: [
      { id: 'pendingKyc', label: 'Pending KYC', link: "/admin/kyc/pending" },
      { id: 'rejectedKyc', label: 'Rejected KYC', link: "/admin/kyc/rejected" },
      { id: 'allKyc', label: 'All KYC Logs', link: "/admin/kyc" },
      { id: 'kycForm', label: 'KYC Form', link: "/" }
    ]},
  ] },
  { id: 3, label: "STAFF MANAGEMENT", subItems: [
    { id: 'manageRoles', label: "Manage Roles", link: "/admin/roles", icon: EnvelopeIcon, },
    { id: 'manageStaffs', label: "Manage Staffs", link: "/admin/staff", icon: EnvelopeIcon, },
  ] },
  { id: 4, label: "PLANS", subItems: [
    { id: 'manageSchema', label: "Manage Schema", icon: EnvelopeIcon,  subItems: [
      { id: 'schedule', label: "Schedule", link: "/admin/schedule", icon: EnvelopeIcon, },
      { id: 'manageSchema', label: "Manage Schema", link: "#", icon: EnvelopeIcon, },
    ] },
  ] },
  { id: 5, label: "TRANSACTIONS", subItems: [
    { id: 'transactions', label: "Transactions", link: "#", icon: ColumnsIcon, },
    { id: 'investments', label: "Investments", link: "#", icon: UsersGroup, },
    { id: 'userProfits', label: "Users Profits", link: "#", icon: UsersIcon, },
  ] },
  { id: 6, label: "ESSENTIALS", subItems: [
    { id: 'automaticGateways', label: "Automatic Gateways", link: "/template/list", icon: ColumnsIcon, },
    { id: 'deposits', label: "Deposits", link: "/group/list", icon: UsersGroup, },
    { id: 'user', label: "Users", link: "/user/list", icon: UsersIcon, },
  ] },
];

const Sidebar = () => {
  
  const toggleCollapseReducer = useSelector(state => state.sidebarReducer.toggleCollapse)

  const router = useRouter();
  const pathname = usePathname();

  const initialDropDownValues = {
    customers: false,
    kyc_management: false,
    manageSchema: false,
    deposits: false,
  }

  const [dropDowns, setDropDowns] = useState(initialDropDownValues)

  const activeMenu = useMemo(() => {
    for (const menuItem of menuItems) {
      const foundMenu = menuItem.subItems?.find((subItem) => subItem.link === pathname);
      if (foundMenu) {
        return foundMenu;
      }
    }
    return null; // Return null if no active menu is found
  }, [pathname]);

  const wrapperClasses = classNames(
    "h-screen px-4 pt-8 pb-4 bg-light flex justify-between flex-col bg-secondaryDark text-white overflow-y-auto scrollbar-none",
    "shadow-xl z-10",
    {
      ["w-80"]: !toggleCollapseReducer,
      ["w-20"]: toggleCollapseReducer,
    }
  );

  const getNavItemClasses = (menu) => {
    return classNames(
      "flex flex-col items-start rounded w-full overflow-hidden whitespace-nowrap transition-all",
    );
  };

  const getItemClasses = (ele) => {
    return classNames(
      `flex py-4 text-sm cursor-pointer hover:bg-light-lighter items-center w-full h-full ${toggleCollapseReducer ? "justify-center" : ""}`,
      {
        ["bg-light-lighter"]: activeMenu?.id === ele.id,
      }
    )
  }

  return (
    <div
      className={wrapperClasses}
      style={{ transition: "width 300ms cubic-bezier(0.2, 0, 0, 1) 0s" }}
    >
      <div className="flex flex-col">
        <div className="flex items-center justify-center relative">
          <div className="flex items-center pl-1 gap-4 rounded-sm">
            {/* <LogoIcon className={toggleCollapseReducer ? "block" : "hidden"}/> */}
            <span
              className="mt-2 text-xl font-bold text-text rounded-sm"
            >
              <img src="/logo.jpeg" alt="logo" className="rounded-sm" />
            </span>
          </div>
        </div>
        <div className="flex flex-col items-start mt-24">
          {menuItems.map((menu, index) => {
            const classes = getNavItemClasses(menu);
            return (
              <div className={classes} key={index}>
                <span className={`text-textGray text-xs  uppercase ${toggleCollapseReducer ? 'hidden' : 'block'}`}>
                  {menu.label}
                </span>
                {menu.subItems &&
                  menu.subItems.map(({ icon: Icon, ...ele2 }, index2) => {
                    return (
                      <>
                        <Link key={index2} href={"#"} legacyBehavior>
                          <a className={getItemClasses(ele2)}>
                            <div className="flex justify-center">
                              <Icon />
                            </div>
                            {!toggleCollapseReducer && (
                              <div className="w-full flex justify-between items-center">
                                <span
                                  className={classNames(
                                    "ml-3 text-md font-medium text-text-light"
                                  )}
                                >
                                {ele2.link ? (
                                  <Link href={ele2.link}>
                                    {ele2.label}
                                  </Link>
                                ) : (
                                  <>
                                    {ele2.label}
                                  </>
                                )}

                                </span>
                                {dropDowns.hasOwnProperty(ele2.id) && (
                                  <button className={`ml-1px-1 text-white font-bold text-lg rounded-sm transition-all transform ${dropDowns[ele2.id] ? 'rotate-0' : 'rotate-180'}`} onClick={() => {
                                    setDropDowns({ ...dropDowns, [ele2.id]: !dropDowns[ele2.id] })
                                  }}>^</button>
                                )}
                              </div>
                            )}
                          </a>
                        </Link>
                        {(!toggleCollapseReducer && dropDowns[ele2.id]) && (
                            <div className="flex flex-col w-full gap-3 bg-[#1E2A4F] p-3 rounded-lg transition-all">
                              {ele2.subItems?.map((val) => {
                                return (
                                  <Link className="text-sm cursor-pointer transition-all hover:bg-[#1E263F] py-1 px-3 rounded-lg" href={val.link}>
                                    {val.label}
                                  </Link>
                                )
                              })}
                            </div>
                        )}

                      </>
                    );
                  })}
                <hr className="w-full my-4 text-start" />
              </div>
            );
          })}
        </div>
      </div>
      {/* logout component */}
      {/* <div className={`${getNavItemClasses({})} px-3 py-4`}>
        <div style={{ width: "2.5rem" }}>
          <LogoutIcon />
        </div>
        {!toggleCollapseReducer && (
          <span className={classNames("text-md font-medium text-text-light")}>
            Logout
          </span>
        )}
      </div> */}
    </div>
  );
};

export default Sidebar;