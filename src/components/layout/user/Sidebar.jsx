"use client"
import classNames from "classnames";
import Link from "next/link";
import { useRouter, usePathname } from "next/navigation";
import React, { useMemo, useState } from "react";
import {
  HomeIcon,
  LogoIcon,
  LogoutIcon,
  UsersGroup,
  ColumnsIcon,
  UsersIcon,
  EnvelopeIcon,
  MessageIcon,
  ChartIcon
} from "../../icons";
import { useSelector } from "react-redux";

import styles from "../../../styles/UserSidebar.module.css"

const menuItems = [
  { id: "dashboard", label: "Dashboard", link: "dashboard" },
  { id: "all-schema", label: "All Schema", link: "all-schema" },
  { id: "schema-logs", label: "Schema Logs", link: "schema-logs" },
  { id: 'all-transactions', label: "All Transactions", link: "all-transactions" },
  { id: "add-money", label: "Add Money", link: "add-money" },
  { id: "add-money-log", label: "Add Money Log", link: "add-money-log" },
  { id: "wallet-exchange", label: "Wallet Exchange", link: "wallet-exchange" },
  { id: "send-money", label: "Send Money", link: "send-money" },
  { id: "withdraw-log", label: "Withdraw Log", link: "withdraw-log" },
  { id: "ranking-badge", label: "Ranking Badge", link: "ranking-badge" },
  { id: "referral", label: "Referral", link: "referral" },
  { id: "settings", label: "Settings", link: "settings" },
  { id: "support-tickets", label: "Support Tickets", link: "support-tickets" },
  { id: "notifications", label: "Notifications", link: "notifications" },
];

const Sidebar = () => {
  
  const toggleCollapseReducer = useSelector(state => state.sidebarReducer.toggleCollapse)

  const router = useRouter();
  const pathname = usePathname();

  console.log(pathname)

  const initialDropDownValues = {
    customers: false,
    kyc_management: false,
    manageSchema: false,
    deposits: false,
  }

  const [dropDowns, setDropDowns] = useState(initialDropDownValues)
  console.log(dropDowns)

  const activeMenu = useMemo(() => {
    for (const menuItem of menuItems) {
      console.log(menuItem)
      const foundMenu = pathname.includes(menuItem.id) 
      console.log(foundMenu)
      if (foundMenu) {
        return menuItem;
      }
    }
    return null; // Return null if no active menu is found
  }, [pathname]);

  console.log(activeMenu)

  const wrapperClasses = classNames(
    "h-screen bg-light flex justify-between flex-col bg-userPrimary text-white overflow-y-auto scrollbar-none",
    "shadow-xl z-10",
    {
      ["w-96 px-4 pt-8 pb-4"]: !toggleCollapseReducer,
      ["w-0"]: toggleCollapseReducer,
    }
  );

  const getNavItemClasses = (menu) => {
    return classNames(
      "flex flex-col items-start rounded w-full overflow-hidden whitespace-nowrap transition-all",
    );
  };

  const getItemClasses = (ele) => {
    return classNames(
      `flex py-4 text-sm cursor-pointer hover:bg-light-lighter items-center w-full h-full ${toggleCollapseReducer ? "justify-center" : ""}`,
      {
        ["bg-light-lighter"]: activeMenu?.id === ele.id,
      }
    )
  }

  return (
    <div
      className={wrapperClasses}
      style={{ transition: "width 300ms cubic-bezier(0.2, 0, 0, 1) 0s" }}
    >
      <div className="flex flex-col">
        <div className="flex items-center justify-center relative">
          <div className="flex items-center pl-1 gap-4 rounded-sm">
            {/* <LogoIcon className={toggleCollapseReducer ? "block" : "hidden"}/> */}
            <span
              className="mt-2 text-xl font-bold text-text rounded-sm"
            >
              <img src="/logo.jpeg" alt="logo" className="rounded-sm" />
            </span>
          </div>
        </div>
        <div className="mt-4 flex flex-col w-full gap-3">
          <div className="flex flex-col p-3 bg-gradient-to-r from-accentRed to-accentOrange rounded-md gap-4">
            <div className="flex justify-between items-center">
              <div className="font-bold text-lg">Account Balance</div>
              <div>
                <button className="bg-white rounded-md text-black text-[9px] font-semibold py-1 px-2">WALLET</button>
              </div>
            </div>

            <div className="flex justify-between items-center">
              <div className="font-bold text-sm">Main Wallet</div>
              <div className="text-white font-bold">
                $0
              </div>
            </div>
            
            <div className="flex justify-between items-center">
              <div className="font-bold text-sm">Profit Wallet</div>
              <div className="text-white font-bold">
                $8
              </div>
            </div>
          </div>
          <div className="flex w-full gap-3 ">
            <div className="w-1/2">
              <button className="w-full bg-blue-600 py-2 rounded-md hover:bg-accentRed transition-all">Deposit</button>
            </div>
            <div className="w-1/2">
              <button className="w-full bg-green-700 py-2 rounded-md hover:bg-accentRed transition-all">Invest Now</button>
            </div>
          </div>
        </div>
        <div className="flex flex-col items-start mt-24 gap-3 px-3">
          {menuItems.map((menu, index) => {
            console.log(menu)
            const classes = getNavItemClasses(menu);
            return (
              <div
                className={`flex flex-col items-start rounded-full w-full transition-all px-6 py-2 cursor-pointer ${activeMenu?.id === menu.id ? 'bg-userSecondary' : `${styles['sidebar-div']}`} `}
                key={index}
              >
                
                <span className={`text-textGray text-sm  uppercase ${toggleCollapseReducer ? 'hidden' : 'block'}`}>
                  <Link href={`/user/${menu.link}`}>
                    {menu.label}
                  </Link>
                </span>
              </div>
            );
          })}
        </div>
      </div>
      {/* logout component */}
      {/* <div className={`${getNavItemClasses({})} px-3 py-4`}>
        <div style={{ width: "2.5rem" }}>
          <LogoutIcon />
        </div>
        {!toggleCollapseReducer && (
          <span className={classNames("text-md font-medium text-text-light")}>
            Logout
          </span>
        )}
      </div> */}
    </div>
  );
};

export default Sidebar;