"use client"
import React, { useState } from "react";
import Sidebar from "./Sidebar";
import Header from "./Header";

const UserLayout = ({ children }) => {

  return (
    <div className="h-screen flex flex-row justify-start relative">
      <Sidebar />
      
      {/* Right side main content section */}
      <div className="w-full flex flex-col justify-center relative bg-userPrimary">
        <Header />
        <div className="flex-1 overflow-y-auto bg-userBg">
          <div className="py-8 px-6">
            {children}
          </div>
        </div>
      </div>
    </div>
  );
};

export default UserLayout;