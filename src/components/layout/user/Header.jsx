import React from 'react'
import DropdownNav from './DropdownNav'
import {
  CollapsIcon
} from "../../icons";
import { setToggleCollapseReducer } from "@/redux/features/sidebar/sidebar-slice";
import { useDispatch, useSelector } from "react-redux";
import classNames from "classnames";

export default function Header() {
  
  const dispatch = useDispatch()

  const toggleCollapseReducer = useSelector(state => state.sidebarReducer.toggleCollapse)

  const collapseIconClasses = classNames(
    "py-2 px-4 rounded bg-userSecondary absolute left-8 transition-all",
    {
      "rotate-180": toggleCollapseReducer,
    }
  );

  const handleSidebarToggle = () => {
    dispatch(setToggleCollapseReducer(!toggleCollapseReducer))
  };

  const menuItems = [
    { id: 1, label: 'Settings', link: '#' },
    { id: 2, label: 'Change Password', link: '#' },
    { id: 3, label: 'Support Tickets', link: '#' },
    // Add more menu items as needed
  ];

  const languageItems = [
    { id: 1, label: 'English', link: '#' },
    { id: 2, label: 'French', link: '#' },
    { id: 3, label: 'Spanish', link: '#' },
    // Add more menu items as needed
  ];

  return (
    <div className='py-4  z-50 bg-userPrimary'>
      <button
        className={collapseIconClasses}
        onClick={handleSidebarToggle}
      >
        <CollapsIcon />
      </button>
      <div className='flex justify-between'>
        <div className='flex justify-center items-center font-semibold'></div>
        <div className='flex'>
          <DropdownNav menuItems={languageItems} isLanguage={true}/>
          <DropdownNav menuItems={menuItems} />
        </div>
      </div>
    </div>
  )
}
