"use client"
import React, {useEffect, useState} from "react";
import DataTable from "react-data-table-component";
import { tableCustomStyles } from "@/styles/TableCustomStyles";

const CustomNoDataComponent = () => (
  <div className="text-center py-12">
    <p className="text-2xl text-gray-400">No data available</p>
  </div>
);

export default function CustomerTableAdmin({ columns, data, ...rest }) {

  const paginationOptions = {
    rowsPerPageText: "Rows per page:", // Customize the displayed text
    rangeSeparatorText: "of", // Customize the displayed text
    selectAllRowsItem: true, // Show an option to select all rows
    selectAllRowsItemText: "All", // Customize the displayed text for "All" option
  };

  const [isMounted, setIsMounted] = useState(false);

  useEffect(() => {
    setIsMounted(true);
  }, []);

  return (
    <div className="w-full rounded-md">
      <DataTable
        persistTableHead
        columns={columns}
        data={data}
        customStyles={tableCustomStyles}
        pagination={isMounted}
        noDataComponent={<CustomNoDataComponent />}
        // paginationComponentOptions={paginationOptions}
      />
    </div>
  );
}
