import { configureStore } from "@reduxjs/toolkit";
import sidebarReducer from "./features/sidebar/sidebar-slice"

export const store = configureStore({
    reducer: {
        sidebarReducer
    }
})