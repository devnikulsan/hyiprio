import { createSlice } from "@reduxjs/toolkit";

const initialState = {
    toggleCollapse: true
}

// this reducer is used to set the state of the main siderbar so whenever the user is redirected or
// page reloads the sidebar toggle state maintains its property
export const sidebartoggle = createSlice({
    name: 'sidebartoggle',
    initialState,
    reducers: {
        setToggleCollapseReducer: (state, action) => {
            return {
                ...state,
                toggleCollapse: action.payload
            }
        }
    }
})

export const { setToggleCollapseReducer } = sidebartoggle.actions
export default sidebartoggle.reducer