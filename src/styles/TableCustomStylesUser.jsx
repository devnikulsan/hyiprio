const tableCustomStylesUser = {
    headRow: {
        style: {
          color:'#223336',
          backgroundColor: '#003049'
        },
      },
      rows: {
        style: {
          color: "#003049",
          backgroundColor: "#003049"
        },
        stripedStyle: {
          color: "#003049",
          backgroundColor: "#003049"
        }
      }
  }
  export { tableCustomStylesUser };