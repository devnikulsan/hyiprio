import React from 'react'
import UserLayout from '@/components/layout/user'
import { CautionIcon } from '@/components/icons'
import { ColumnsIcon } from '@/components/icons'

export default function page() {


  const cards = [
    {
      name: "All Transactions",
      value: "0"
    },
    {
      name: "Total Deposit",
      value: "0"
    },
    {
      name: "Total Investment",
      value: "0"
    },
    {
      name: "Total Profit",
      value: "0"
    },
    {
      name: "Total Transfer",
      value: "0"
    },
    {
      name: "Total Withdraw",
      value: "0"
    },
    {
      name: "Referral Bonus",
      value: "0"
    },
    {
      name: "Deposit Bonus",
      value: "0"
    },
    {
      name: "Investment Bonus",
      value: "0"
    },
    {
      name: "Total Referral",
      value: "0"
    },
    {
      name: "Rank Achieved",
      value: "0"
    },
    {
      name: "Total Ticket",
      value: "0"
    }
  ]

  return (
    <UserLayout>
        <div className='px-2 py-4 flex flex-col gap-5'>
          <div className='bg-userPrimary py-3 px-4 rounded-md flex'>
            <div className='w-4/6 flex items-center gap-3 text-white'>
              <div className='rounded-full p-3 bg-yellow-700'>
               <CautionIcon fill="bg-yellow-700"/>
              </div>
              <div>You need to submit your <b>KYC and Other Documents</b> before proceed to the system.</div>
            </div>
            <div className='w-2/6 flex items-center justify-end gap-3'>
              <div className='flex justify-center items-center bg-gradient-to-r from-accentRed to-accentOrange px-3 py-1 rounded-xl cursor-pointer hover:to-accentRed hover:-translate-y-1 transition-all'>
                <button className='text-xs text-white'>SUBMIT NOW</button>
              </div>
              <div className='flex justify-center items-center bg-accentRed px-3 py-1 rounded-xl cursor-pointer hover:bg-userPrimary hover:-translate-y-1 transition-all'>
                <button className='text-xs text-white'>X &nbsp; LATER</button>
              </div>
            </div>
          </div>

          <div className='flex'>
            <div className='flex justify-start h-[250px] items-center p-6'>
              <div className='bg-userPrimary rounded-full flex flex-col w-44 h-44 justify-center items-center py-12'>
                <div className='text-white font-bold text-sm'>Level 1</div>
                <div className='text-gray-400 font-bold text-xs'>Hyip Member</div>
              </div>
            </div>
            <div className='w-3/4 py-6 flex flex-col justify-center'>
              <div className='rounded-md bg-userDark flex px-3 py-5 shadow-xl border border-gray-600'>
                <div className='text-white'>Referral URL</div>
              </div>
              <div className='px-3 py-5 shadow-xl border border-gray-600 rounded-md bg-userDark'>
                <div className='flex rounded-md'>
                  <div className='w-3/4'>
                    <input value={"https://amyrafarms.co/register?invite=abc123"} className='w-full outline-none bg-userDark border border-gray-500 rounded-l-md text-white px-3 py-2' type="text" />
                  </div>
                  <div className='w-1/4 flex justify-center items-center cursor-pointer'>
                    <div className='w-full outline-none bg-accentRed border border-gray-500 rounded-r-md text-white px-3 py-2 text-center' type="text">Copy</div>
                  </div>
                </div>
                <div className='py-2 text-white text-sm'>
                  0 peoples are joined by using this URL
                </div>
              </div>

            </div>


          </div>
          <div className='px-6 my-4 grid grid-cols-4 gap-4'>
            {cards.map((val, index) => {
              return (
                <div key={index} className='relative overflow-hidden bg-[#535a94] w-full flex p-4 gap-5 rounded-lg border-2 border-[#a98f4a] hover:-translate-y-1 transition-all'>
                  <div className='absolute top-[-12px] right-[-12px] bg-[#a98f4a] rounded-full p-2 w-12 h-12'></div>
                  <div className='flex justify-center items-center w-1/4 text-black bg-white rounded-full'>
                    <ColumnsIcon />
                  </div>
                  <div className='flex flex-col w-3/4 justify-start gap-2 text-white font-semibold'>
                    <div className='flex justify-start'>
                      {val.value}
                    </div>
                    <div>
                      {val.name}
                    </div>
                  </div>
                </div>
              );
            })}
          </div>
        </div>
    </UserLayout>
  )
}
