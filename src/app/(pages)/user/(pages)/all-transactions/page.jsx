import React from 'react'
import UserLayout from '@/components/layout/user'
import { CautionIcon } from '@/components/icons'
import CustomerTableUser from '@/components/tables/user/CustomTableUser'

export default function page() {
  return (
    <UserLayout>
        <div className='bg-userPrimary py-3 px-4 rounded-md flex'>
        <div className='w-4/6 flex items-center gap-3 text-white'>
            <div className='rounded-full p-3 bg-yellow-700'>
            <CautionIcon fill="bg-yellow-700"/>
            </div>
            <div>You need to submit your <b>KYC and Other Documents</b> before proceed to the system.</div>
        </div>
        <div className='w-2/6 flex items-center justify-end gap-3'>
            <div className='flex justify-center items-center bg-gradient-to-r from-accentRed to-accentOrange px-3 py-1 rounded-xl cursor-pointer hover:to-accentRed hover:-translate-y-1 transition-all'>
            <button className='text-xs text-white'>SUBMIT NOW</button>
            </div>
            <div className='flex justify-center items-center bg-accentRed px-3 py-1 rounded-xl cursor-pointer hover:bg-userPrimary hover:-translate-y-1 transition-all'>
            <button className='text-xs text-white'>X &nbsp; LATER</button>
            </div>
        </div>
        </div>


    </UserLayout>
  )
}
