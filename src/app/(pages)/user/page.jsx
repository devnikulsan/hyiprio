import React from 'react'
import UserLayout from '@/components/layout/user'

export default function page() {
  return (
    <UserLayout>
                <div className='flex justify-center items-center bg-white rounded-md p-5'>
        User Layout
        </div>
    </UserLayout>
  )
}
