import React from 'react';
import Image from 'next/image'

export default function AdminLogin() {
  return (
    <div className='h-screen bg-secondary flex justify-center items-center'>
      <div className='flex bg-white w-full h-[60%] mx-64 rounded-md'>
        <div className='w-1/2 bg-gray-400 flex rounded-l-lg z-0 justify-center items-center'>
            <div className='z-10 bg-gray-300 w-full flex justify-center items-center py-6 capitalize text-white text-3xl font-bold'>
                admin login
            </div>
        </div>
        <div className='w-1/2 h-full'>
            <div className='flex flex-col justify-center px-12 h-full gap-4'>
                <Image
                    src="/logo.jpeg"
                    alt="App Logo"
                    width={200}
                    height={75}
                    priority
                />
                <div className='mt-3'>
                    <label htmlFor='email' className='block text-lg font-semibold text-gray-700'>
                        Admin Email
                    </label>
                    <input
                        type='text'
                        id='email'
                        className='mt-1 py-2 px-3 border border-gray-300 rounded-md w-full outline-none placeholder:text-sm text-black'
                        placeholder='Admin Email'
                    />
                </div>
                <div className='mt-3'>
                    <label htmlFor='password' className='block text-lg font-semibold text-gray-700 '>
                        Password
                    </label>
                    <input
                        type='password'
                        id='password'
                        className='mt-1 py-2 px-3 border border-gray-300 rounded-md w-full outline-none placeholder:text-sm text-black'
                        placeholder='Password'
                    />
                </div>

                <button className='mt-3 uppercase bg-primary text-white py-3 rounded-md font-semibold hover:shadow-lg transition-all text-sm'>
                    admin login
                </button>

                <div className='capitalize font-semibold text-black text-sm cursor-pointer'>
                    forget password?
                </div>
            </div>
        </div>
      </div>
    </div>
  );
}