"use client";

import React from "react";
import AdminLayout from "@/components/layout/admin";
import CustomerTableAdmin from "@/components/tables/admin/customer/page";
import { PencilIcon } from "@/components/icons";

export default function AllCustomers() {

  const columns = [
    { name: "User", selector: "user" },
    { name: "Email", selector: "email" },
    { name: "Balance", selector: "balance" },
    { name: "Profit", selector: "profit" },
    { name: "KYC", selector: "kyc" },
    { name: "Status", selector: "status" },
    { name: "Action", selector: "action" },
    // Add more columns as needed
  ];

  const getStatusStyle = (status) => {
    // Define your conditions to set the status style based on the 'status'
    if (status === "Active") {
      return <div className="bg-green-500 text-white px-3 py-1 rounded-lg">Active</div>;
    } else if (status === "Inactive") {
      return <div className="bg-yellow-500 text-white px-3 py-1 rounded-lg">Inactive</div>;
    } else {
      return <div className="bg-gray-400 text-white px-3 py-1 rounded-lg">Pending</div>;
    }
  };

  const getKYCStyle = (kyc) => {
    // Define your conditions to set the kyc style based on the 'kyc'
    if (kyc === "Verified") {
      return <div className="bg-green-500 text-white px-3 py-1 rounded-lg">Verified</div>;
    } else if (kyc === "Unverified") {
      return <div className="bg-yellow-500 text-black px-3 py-1 rounded-lg">Unverified</div>;
    } else {
      return <div className="bg-gray-400 text-white px-3 py-1 rounded-lg">Pending</div>;
    }
  };
  
  function getRandomString(str1, str2) {
    const randomNumber = Math.random();
    return randomNumber < 0.5 ? str1 : str2;
  }
  
  console.log('asd',getRandomString("Active", "Inactive"))

  const buttonClassName = "bg-accentRed text-white p-2 rounded-full hover:shadow-lg"

  const data = [
    {
      user: "John Doe",
      email: "john.doe@example.com",
      balance: <span>1000 USD</span>,
      profit: <span>500 USD</span>,
      kyc: getKYCStyle("Unverified"),
      status: getStatusStyle(getRandomString("Active", "Inactive")),
      action: (
        <div className="rounded-full p-2">
            <button className={buttonClassName}><PencilIcon width={12}/></button>
        </div>
      ),
    },
    {
      user: "Jane Smith",
      email: "jane.smith@example.com",
      balance: <span>1000 USD</span>,
      profit: <span>500 USD</span>,
      kyc: getKYCStyle("Unverified"),
      status: getStatusStyle("Inactive"),
      action: (
        <div className="rounded-full p-2">
            <button className={buttonClassName}><PencilIcon width={12}/></button>
        </div>
      ),
    },
    {
      user: "Michael Johnson",
      email: "michael.johnson@example.com",
      balance: <span>1000 USD</span>,
      profit: <span>500 USD</span>,
      kyc: getKYCStyle("Unverified"),
      status: getStatusStyle("Active"),
      action: (
        <div className="rounded-full p-2">
            <button className={buttonClassName}><PencilIcon width={12}/></button>
        </div>
      ),
    },
    {
      user: "Emily Adams",
      email: "emily.adams@example.com",
      balance: <span>1000 USD</span>,
      profit: <span>500 USD</span>,
      kyc: getKYCStyle("Unverified"),
      status: getStatusStyle("Active"),
      action: (
        <div className="rounded-full p-2">
            <button className={buttonClassName}><PencilIcon width={12}/></button>
        </div>
      ),
    },
    {
      user: "David Wilson",
      email: "david.wilson@example.com",
      balance: <span>1000 USD</span>,
      profit: <span>500 USD</span>,
      kyc: getKYCStyle("Unverified"),
      status: getStatusStyle("Active"),
      action: (
        <div className="rounded-full p-2">
            <button className={buttonClassName}><PencilIcon width={12}/></button>
        </div>
      ),
    },
    {
      user: "Sophia Lee",
      email: "sophia.lee@example.com",
      balance: <span>1000 USD</span>,
      profit: <span>500 USD</span>,
      kyc: getKYCStyle("Unverified"),
      status: getStatusStyle("Inactive"),
      action: (
        <div className="rounded-full p-2">
            <button className={buttonClassName}><PencilIcon width={12}/></button>
        </div>
      ),
    },
    {
      user: "William Brown",
      email: "william.brown@example.com",
      balance: <span>1000 USD</span>,
      profit: <span>500 USD</span>,
      kyc: getKYCStyle("Unverified"),
      status: getStatusStyle("Active"),
      action: (
        <div className="rounded-full p-2">
            <button className={buttonClassName}><PencilIcon width={12}/></button>
        </div>
      ),
    },
    {
      user: "Olivia Davis",
      email: "olivia.davis@example.com",
      balance: <span>1000 USD</span>,
      profit: <span>500 USD</span>,
      kyc: getKYCStyle("Unverified"),
      status: getStatusStyle("Active"),
      action: (
        <div className="rounded-full p-2">
            <button className={buttonClassName}><PencilIcon width={12}/></button>
        </div>
      ),
    },
    {
      user: "James Martinez",
      email: "james.martinez@example.com",
      balance: <span>1000 USD</span>,
      profit: <span>500 USD</span>,
      kyc: getKYCStyle("Unverified"),
      status: getStatusStyle("Inactive"),
      action: (
        <div className="rounded-full p-2">
            <button className={buttonClassName}><PencilIcon width={12}/></button>
        </div>
      ),
    },
    {
      user: "Ava Robinson",
      email: "ava.robinson@example.com",
      balance: <span>1000 USD</span>,
      profit: <span>500 USD</span>,
      kyc: getKYCStyle("Unverified"),
      status: getStatusStyle("Active"),
      action: (
        <div className="rounded-full p-2">
            <button className={buttonClassName}><PencilIcon width={12}/></button>
        </div>
      ),
    },
  ];

  return (
    <AdminLayout>
      <div className="flex flex-col w-full gap-6">
        <div className="flex justify-between items-center">
            <div className="text-2xl font-bold">All Customers</div>
            <div className="">
                <span className="mr-2">Search:</span> <input type="text" className="text-sm rounded-sm outline-none py-1 px-3 border border-gray-300 " />
            </div>
        </div>
        <CustomerTableAdmin data={data} columns={columns}/>
      </div>
    </AdminLayout>
  );
}
