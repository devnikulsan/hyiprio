import React from 'react'
import AdminLayout from '@/components/layout/admin'

export default function page() {
  return (
    <AdminLayout>
        <div className='font-bold text-3xl'>
            Send Email To All
        </div>

        <div className='mt-4 bg-white rounded-md shadow-md p-4 flex flex-col gap-4'>
            <div className='flex flex-col gap-2'>
                <label htmlFor="input1">Subject</label>
                <input className='outline-none border border-gray-300 hover:border-primary focus:shadow-md focus:border-primary rounded-sm p-3 transition-all' type="text" />
            </div>
            <div className='flex flex-col gap-2'>
                <label htmlFor="input2">Email Details</label>
                <textarea className='outline-none border border-gray-300 hover:border-primary focus:shadow-md focus:border-primary rounded-sm p-3 transition-all' name="" id="" rows="4"></textarea>
            </div>
            
            <div className='mt-4'>
                <button className='bg-primary py-2 px-5 hover:shadow-xl transition-all text-white rounded-sm'>Send Email</button>
            </div>
        </div>


    </AdminLayout>
  )
}
