"use client"
import { useParams } from 'next/navigation';
import React from "react";
import AdminLayout from "@/components/layout/admin";
import CustomerTableAdmin from "@/components/tables/admin/customer/page";
import { EyeIcon } from "@/components/icons";

export default function AllCustomers() {

  const slug = ""

  const columns = [
    { name: "Date", selector: "date" },
    { name: "User", selector: "user" },
    { name: "Type", selector: "type" },
    { name: "Status", selector: "status" },
    { name: "Action", selector: "action" },
    // Add more columns as needed
  ];

  const getStatusStyle = (status) => {
    // Define your conditions to set the status style based on the 'status'
    if (status === "Rejected") {
      return <div className="bg-red-500 text-white px-4 py-1 rounded-full">Rejected</div>;
    } else if (status === "Pending") {
      return <div className="bg-yellow-500 text-black px-4 py-1 rounded-full">Pending</div>;
    } else {
      return <div className="bg-green-600 text-white px-4 py-1 rounded-full">Verified</div>;
    }
  };

  const getKYCStyle = (kyc) => {
    // Define your conditions to set the kyc style based on the 'kyc'
    if (kyc === "Verified") {
      return <div className="bg-green-500 text-white px-3 py-1 rounded-lg">Verified</div>;
    } else if (kyc === "Unverified") {
      return <div className="bg-yellow-500 text-black px-3 py-1 rounded-lg">Unverified</div>;
    } else {
      return <div className="bg-gray-400 text-white px-3 py-1 rounded-lg">Pending</div>;
    }
  };



  
  const buttonClassName = "bg-primary text-white p-2 rounded-full hover:shadow-lg"

  const data = [
    {
      date: "2023-07-20",
      user: "John Doe",
      type: "NATIONAL ID VERIFICATION",
      status: getStatusStyle("Pending"),
            action: (
        <div className="rounded-full p-2">
            <button className={buttonClassName}><EyeIcon width={12}/></button>
        </div>
      ),
    },
    {
      date: "2023-07-19",
      user: "Jane Smith",
      type: "NATIONAL ID VERIFICATION",
      status: getStatusStyle("Pending"),
            action: (
        <div className="rounded-full p-2">
            <button className={buttonClassName}><EyeIcon width={12}/></button>
        </div>
      ),
    },
    {
      date: "2023-07-18",
      user: "Michael Johnson",
      type: "NATIONAL ID VERIFICATION",
      status: getStatusStyle("Pending"),
            action: (
        <div className="rounded-full p-2">
            <button className={buttonClassName}><EyeIcon width={12}/></button>
        </div>
      ),
    },
    {
      date: "2023-07-17",
      user: "Emily Adams",
      type: "NATIONAL ID VERIFICATION",
      status: getStatusStyle("Pending"),
            action: (
        <div className="rounded-full p-2">
            <button className={buttonClassName}><EyeIcon width={12}/></button>
        </div>
      ),
    },
    {
      date: "2023-07-16",
      user: "David Wilson",
      type: "NATIONAL ID VERIFICATION",
      status: getStatusStyle("Pending"),
            action: (
        <div className="rounded-full p-2">
            <button className={buttonClassName}><EyeIcon width={12}/></button>
        </div>
      ),
    },
    {
      date: "2023-07-15",
      user: "Sophia Lee",
      type: "NATIONAL ID VERIFICATION",
      status: getStatusStyle("Pending"),
            action: (
        <div className="rounded-full p-2">
            <button className={buttonClassName}><EyeIcon width={12}/></button>
        </div>
      ),
    },
    {
      date: "2023-07-14",
      user: "William Brown",
      type: "NATIONAL ID VERIFICATION",
      status: getStatusStyle("Pending"),
            action: (
        <div className="rounded-full p-2">
            <button className={buttonClassName}><EyeIcon width={12}/></button>
        </div>
      ),
    },
    {
      date: "2023-07-13",
      user: "Olivia Davis",
      type: "NATIONAL ID VERIFICATION",
      status: getStatusStyle("Pending"),
            action: (
        <div className="rounded-full p-2">
            <button className={buttonClassName}><EyeIcon width={12}/></button>
        </div>
      ),
    },
    {
      date: "2023-07-12",
      user: "James Martinez",
      type: "NATIONAL ID VERIFICATION",
      status: getStatusStyle("Verified"),
            action: (
        <div className="rounded-full p-2">
            <button className={buttonClassName}><EyeIcon width={12}/></button>
        </div>
      ),
    },
    {
      date: "2023-07-11",
      user: "Ava Robinson",
      type: "NATIONAL ID VERIFICATION",
      status: getStatusStyle("Pending"),
            action: (
        <div className="rounded-full p-2">
            <button className={buttonClassName}><EyeIcon width={12}/></button>
        </div>
      ),
    },
    // Add more data objects as needed
  ];

  return (
    <AdminLayout>
      <div className="flex flex-col w-full gap-6">
        <div className="flex justify-between items-center">
            <div className="text-2xl font-bold capitalize">All KYC</div>
            <div className="">
                <span className="mr-2">Search:</span> <input type="text" className="text-sm rounded-sm outline-none py-1 px-3 border border-gray-300 " />
            </div>
        </div>
        <CustomerTableAdmin data={data} columns={columns}/>
      </div>
    </AdminLayout>
  );
}
