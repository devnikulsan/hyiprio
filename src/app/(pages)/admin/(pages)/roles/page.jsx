import React from 'react'
import AdminLayout from '@/components/layout/admin'
import CustomerTableAdmin from '@/components/tables/admin/customer/page'
import { PencilIcon, EyeIcon } from "@/components/icons";

export default function page() {
    const columns = [
        { name: "#", selector: "no" },
        { name: "Name", selector: "name" },
        { name: "Action", selector: "action" },
        // Add more columns as needed
      ];
      const buttonClassName = "flex items-center gap-2 bg-accentRed text-white p-2 rounded-md hover:shadow-lg"

      const data = [
        {
          no: "1",
          name: "Super Admin",
          action: (
            <div className="rounded-full p-2">
                <button className={buttonClassName}>Not Editable</button>
            </div>
          ),
        },
        {
          no: "2",
          name: "Manager",
          action: (
            <div className="rounded-full p-2">
                <button className="flex items-center gap-2 bg-primary text-white p-2 rounded-md hover:shadow-lg">
                    <div>
                        <PencilIcon width={12}/>
                    </div>
                    <div>
                        Edit Permission
                    </div>
                </button>
            </div>
          ),
        },
        {
          no: "3",
          name: "Editor",
          action: (
            <div className="rounded-full p-2">
                <button className="flex items-center gap-2 bg-primary text-white p-2 rounded-md hover:shadow-lg">
                    <div>
                        <PencilIcon width={12}/>
                    </div>
                    <div>
                        Edit Permission
                    </div>
                </button>
            </div>
          ),
        }
      ];
  return (
    <AdminLayout>
        <div className='flex items-center justify-between'>
            <div className='font-bold text-3xl'>
                Manage Roles
            </div>
            <div>
                <button className="flex items-center gap-2 bg-primary text-white py-2 px-3 text-sm rounded-md hover:shadow-lg">
                    <div>
                        Add New Role
                    </div>
                </button> 
            </div>
        </div>

        <div className='mt-4 bg-white rounded-md shadow-md p-4 flex flex-col gap-4'>

            <CustomerTableAdmin data={data} columns={columns}/>
        </div>


    </AdminLayout>
  )
}
