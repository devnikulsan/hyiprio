import React from 'react'
import AdminLayout from '@/components/layout/admin'
import { UsersIcon } from '@/components/icons';

export default function Notification() {


    const getAvatar = () => {
        // Generate a random color
        const randomColor = "#" + Math.floor(Math.random() * 16777215).toString(16);
      
        // Return a circle div with the random color as the background
        return (
          <div
            style={{
              width: "40px",
              height: "40px",
              borderRadius: "50%",
              backgroundColor: randomColor,
            }}
            className='flex justify-center items-center text-xs text-white'
          ><UsersIcon /></div>
        );
      };

    const notificationsData = [
        {
          message: "Thanks for joining us opeyemi taiwo .New User added our system.",
          timestamp: "22 minutes ago",
          type: "New User",
          avatar: getAvatar()
        },
        {
          message: "Thanks for joining us Muhammad Imad Hassan .New User added our system.",
          timestamp: "1 hour ago",
          type: "New User",
          avatar: getAvatar()
        },
        {
          message: "Thanks for joining us Mohammed Ansar .New User added our system.",
          timestamp: "1 hour ago",
          type: "New User",
          avatar: getAvatar()
        },
        {
          message: "Roshan Ranasinghe Kyc requested ranasinghexp@gmail.com",
          timestamp: "2 hours ago",
          type: "KYC Request",
          avatar: getAvatar()
        },
        {
          message: "Thanks for joining us Razeen Naleem .New User added our system.",
          timestamp: "2 hours ago",
          type: "New User",
          avatar: getAvatar()
        },
        {
          message: "Thanks for joining us Nimra Zai .New User added our system.",
          timestamp: "2 hours ago",
          type: "New User",
          avatar: getAvatar()
        },
        {
          message: "Thanks for joining us L.P.L.Sangeeth Sangeeth .New User added our system.",
          timestamp: "5 hours ago",
          type: "New User",
          avatar: getAvatar()
        },
        {
          message: "Thanks for joining us Roshan Ranasinghe .New User added our system.",
          timestamp: "5 hours ago",
          type: "New User",
          avatar: getAvatar()
        },
        {
          message: "Thanks for joining us Pajaj Qbssjs .New User added our system.",
          timestamp: "5 hours ago",
          type: "New User",
          avatar: getAvatar()
        },
        {
          message: "Thanks for joining us xolo xolo .New User added our system.",
          timestamp: "5 hours ago",
          type: "New User",
          avatar: getAvatar()
        },
        {
            message: "Thanks for joining us Roshan Ranasinghe .New User added our system.",
            timestamp: "5 hours ago",
            type: "New User",
            avatar: getAvatar()
          },
          {
            message: "Thanks for joining us Pajaj Qbssjs .New User added our system.",
            timestamp: "5 hours ago",
            type: "New User",
            avatar: getAvatar()
          },
          {
            message: "Thanks for joining us xolo xolo .New User added our system.",
            timestamp: "5 hours ago",
            type: "New User",
            avatar: getAvatar()
          },
      ];

  return (
    <AdminLayout>
        <div className="flex flex-col w-full gap-6">
            <div className="flex justify-between items-center">
                <div className="text-2xl font-bold capitalize">All Notifications</div>
                <div className="">
                    <button className='bg-secondary text-white px-3 py-1 rounded-md hover:shadow-lg transition-all'>
                        Mark All Read
                    </button>
                </div>
            </div>
            
            <div className='bg-white rounded-sm border border-gray-300 p-3 shadow-lg'>
                <div className='flex flex-col bg-gray-200 transition-all'>
                    {notificationsData.map((val) => {
                        return (
                            <div className='flex justify-between items-center px-3 py-4 hover:bg-gray-300 transition-all'>
                                <div className='flex  gap-3'>
                                    {val.avatar}
                                    <div className='flex flex-col'>
                                        <div className='text-sm font-semibold'>{val.message}</div>
                                        <div className='text-xs text-gray-400'>{val.timestamp}</div>
                                    </div>
                                </div>
                                <div className='flex justify-center items-center'>
                                    <button className='bg-accentRed py-2 px-5 text-white rounded-sm text-xs hover:shadow-lg'>Explore</button>
                                </div>
                            </div>
                        )
                    })}
                </div>
            </div>
        </div>

    </AdminLayout>
  )
}
