import React from 'react'
import AdminLayout from '@/components/layout/admin'
import CustomerTableAdmin from '@/components/tables/admin/customer/page'
import { PencilIcon, EyeIcon } from "@/components/icons";

export default function page() {

    const getStatusStyle = (status) => {
        // Define your conditions to set the status style based on the 'status'
        if (status === "Active") {
          return <div className="bg-green-500 text-white px-3 py-1 rounded-lg">Active</div>;
        } else if (status === "Inactive") {
          return <div className="bg-yellow-500 text-white px-3 py-1 rounded-lg">Inactive</div>;
        } else {
          return <div className="bg-gray-400 text-white px-3 py-1 rounded-lg">Pending</div>;
        }
      };
      
    const columns = [
        { name: "#", selector: "no" },
        { name: "Name", selector: "name" },
        { name: "Time", selector: "time" },
        { name: "Action", selector: "action" },
        // Add more columns as needed
      ];
      const buttonClassName = "flex items-center gap-2 bg-primary text-white p-2 rounded-full hover:shadow-lg"

      const data = [
        {
          no: "1",
          name: "Hour",
          time: "1 Hour",
        action: (
            <div className="rounded-full p-2">
                <button className={buttonClassName}><PencilIcon width={12}/></button>
            </div>
          ),
        },
        {
          no: "2",
          name: "Daily",
          time: "24 Hours",
          action: (
              <div className="rounded-full p-2">
                  <button className={buttonClassName}><PencilIcon width={12}/></button>
              </div>
            ),
          },
          {
            no: "3",
            name: "Weekly",
            time: "168 Hours",
            action: (
                <div className="rounded-full p-2">
                    <button className={buttonClassName}><PencilIcon width={12}/></button>
                </div>
              ),
            },
      ];
  return (
    <AdminLayout>
        <div className='flex items-center justify-between'>
            <div className='font-bold text-3xl'>
            All Schedules
            </div>
            <div>
                <button className="flex items-center gap-2 bg-primary text-white py-2 px-3 text-sm rounded-md hover:shadow-lg">
                    <div>
                        Add New
                    </div>
                </button> 
            </div>
        </div>

        <div className='mt-4 bg-white rounded-md shadow-md p-4 flex flex-col gap-4'>

            <CustomerTableAdmin data={data} columns={columns}/>
        </div>


    </AdminLayout>
  )
}
