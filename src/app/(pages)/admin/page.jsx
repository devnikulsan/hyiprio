import React from 'react'
import AdminLayout from '@/components/layout/admin'

export default function Admin() {
  return (
    <AdminLayout>
        <div className='p-6 bg-white rounded-lg'>
            Dashboard
        </div>
    </AdminLayout>
  )
}
