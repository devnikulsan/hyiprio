"use client"
import React from 'react'
import Image from 'next/image'
import PhoneNumberInput from '@/components/common/PhoneInput'

export default function Register() {
  return (
    <div className='md:h-full flex justify-center items-center bg-primary'>
        <div className='flex flex-col justify-evenly bg-white w-full my-10 md:my-auto mx-6 sm:mx-42 md:mx-54 lg:mx-64 xl:mx-82 2xl:mx-[500px] rounded-xl py-12 px-12'>
            <Image
                src="/logo.jpeg"
                alt="App Logo"
                width={200}
                height={75}
                priority
            />

            <div className='mt-12 flex flex-col gap-3'>
                <div className='text-3xl font-semibold'>
                    Create an account
                </div>

                <div>
                    Register to continue with Hyiprio
                </div>
            </div>

            <div className='mt-6 flex flex-col md:flex-row w-full justify-evenly gap-3'>
                <div className='w-full md:w-1/2'>
                    <label htmlFor='email' className='block text-gray-700'>
                        First Name <span className='text-red-500'>*</span>
                    </label>
                    <input
                        type='text'
                        id='email'
                        className='mt-1 py-2 px-3 border border-gray-300 rounded-md w-full outline-none placeholder:text-sm text-black'
                        placeholder='Enter your first name'
                    />
                </div>
                <div className='w-full md:w-1/2'>
                    <label htmlFor='email' className='block text-gray-700'>
                        Last Name <span className='text-red-500'>*</span>
                    </label>
                    <input
                        type='text'
                        id='email'
                        className='mt-1 py-2 px-3 border border-gray-300 rounded-md w-full outline-none placeholder:text-sm text-black'
                        placeholder='Enter your last name'
                    />
                </div>
            </div>

            <div className='mt-6 flex flex-col md:flex-row w-full justify-evenly gap-3'>
                <div className='w-full md:w-1/2'>
                    <label htmlFor='email' className='block text-gray-700'>
                        Email Address <span className='text-red-500'>*</span>
                    </label>
                    <input
                        type='email'
                        id='email'
                        className='mt-1 py-2 px-3 border border-gray-300 rounded-md w-full outline-none placeholder:text-sm text-black'
                        placeholder='Enter your first email address'
                    />
                </div>
                <div className='w-full md:w-1/2'>
                    <label htmlFor='email' className='block text-gray-700'>
                        User Name <span className='text-red-500'>*</span>
                    </label>
                    <input
                        type='text'
                        id='email'
                        className='mt-1 py-2 px-3 border border-gray-300 rounded-md w-full outline-none placeholder:text-sm text-black'
                        placeholder='Enter your user name'
                    />
                </div>
            </div>

            <div className='mt-6 flex flex-col md:flex-row w-full justify-evenly gap-3'>
                <div className='w-full md:w-1/2'>
                    <label htmlFor='email' className='block text-gray-700'>
                        Email Address <span className='text-red-500'>*</span>
                    </label>
                    <input
                        type='email'
                        id='email'
                        className='mt-1 py-2 px-3 border border-gray-300 rounded-md w-full outline-none placeholder:text-sm text-black'
                        placeholder='Enter your first email address'
                    />
                </div>
                <div className='w-full md:w-1/2'>
                    <label htmlFor='email' className='block text-gray-700'>
                        Phone Number <span className='text-red-500'>*</span>
                    </label>
                    <PhoneNumberInput />
                </div>
            </div>

            <div className='mt-6 flex flex-col md:flex-row w-full justify-evenly gap-3'>
                <div className='w-full md:w-1/2'>
                    <label htmlFor='password' className='block text-gray-700 '>
                        Password
                    </label>
                    <input
                        type='password'
                        id='password'
                        className='mt-1 py-2 px-3 border border-gray-300 rounded-md w-full outline-none placeholder:text-sm text-black'
                        placeholder='Enter your password'
                    />
                </div>
                <div className='w-full md:w-1/2'>
                    <label htmlFor='confirmPassword' className='block text-gray-700 '>
                        Confirm Password
                    </label>
                    <input
                        type='confirmPassword'
                        id='confirmPassword'
                        className='mt-1 py-2 px-3 border border-gray-300 rounded-md w-full outline-none placeholder:text-sm text-black'
                        placeholder='Enter your password'
                    />
                </div>
            </div>

            <div className='italic mt-6'>recaptcha component</div>

            <div className='flex mt-6 transition-all items-center justify-between'>
                <div className='flex items-center'>
                    <input type="checkbox" className="h-[1.125rem] w-[1.125rem] appearance-none rounded-[0.25rem] border-[0.125rem] border-solid border-neutral-300 outline-none before:pointer-events-none before:absolute before:h-[0.875rem] before:w-[0.875rem] before:scale-0 before:rounded-full before:bg-transparent before:opacity-0 before:shadow-[0px_0px_0px_13px_transparent] before:content-[''] checked:border-red-500 checked:bg-red-500 checked:before:opacity-[0.16] checked:after:absolute checked:after:-mt-px checked:after:ml-[0.25rem] checked:after:block checked:after:h-[0.8125rem] checked:after:w-[0.375rem] checked:after:rotate-45 checked:after:border-[0.125rem] checked:after:border-l-0 checked:after:border-t-0 checked:after:border-solid checked:after:border-white checked:after:bg-transparent checked:after:content-[''] hover:cursor-pointer hover:before:opacity-[0.04] hover:before:shadow-[0px_0px_0px_13px_rgba(0,0,0,0.6)] focus:shadow-none focus:transition-[border-color_0.2s] focus:before:scale-100 focus:before:opacity-[0.12] focus:before:shadow-[0px_0px_0px_13px_rgba(0,0,0,0.6)] focus:before:transition-[box-shadow_0.2s,transform_0.2s] focus:after:absolute focus:after:z-[1] focus:after:block focus:after:h-[0.875rem] focus:after:w-[0.875rem] focus:after:rounded-[0.125rem] focus:after:content-[''] checked:focus:before:scale-100 checked:focus:before:shadow-[0px_0px_0px_13px_#3b71ca] checked:focus:before:transition-[box-shadow_0.2s,transform_0.2s] checked:focus:after:-mt-px checked:focus:after:ml-[0.25rem] checked:focus:after:h-[0.8125rem] checked:focus:after:w-[0.375rem] checked:focus:after:rotate-45 checked:focus:after:rounded-none checked:focus:after:border-[0.125rem] checked:focus:after:border-l-0 checked:focus:after:border-t-0 checked:focus:after:border-solid checked:focus:after:border-white checked:focus:after:bg-transparent" />
                    <label className="checkbox ml-3">
                        I agree with&nbsp;
                        <span className='text-accentRed hover:text-black drop-shadow-md hover:drop-shadow-lg transition-all cursor-pointer'>Privacy & Policy</span>
                        &nbsp;and&nbsp;
                        <span className='text-accentRed hover:text-black drop-shadow-md hover:drop-shadow-lg transition-all cursor-pointer'>Terms & Condition</span>
                    </label>
                </div>
            </div>

            <div className='mt-6 w-full flex flex-col justify-center items-center gap-3'>
                <button className='w-full rounded-full py-3 text-white text-sm uppercase bg-gradient-to-r from-accentRed to-accentOrange hover:-translate-y-1 transition-all hover:bg-accentRed'>
                    create account
                </button>

                <div className='w-full flex justify-center items-center'>
                    Already have an account?&nbsp;<span className='text-accentRed hover:text-black drop-shadow-md hover:drop-shadow-lg transition-all cursor-pointer'>Login</span>
                </div>
            </div>
        </div>
    </div>
  )
}
