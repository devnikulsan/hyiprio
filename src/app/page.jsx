import Image from 'next/image'
import UserLayout from '@/components/layout/user'
import Link from "next/link";

export default function Home() {
  return (
    <div className='flex flex-col gap-3 justify-center items-center h-full bg-userPrimary'>
      <div className='text-4xl font-semibold bg-white p-6 rounded-lg'>Main Landing Page</div>
      <div className='flex gap-4 bg-white px-6 py-3 rounded-lg'>For User Panel <div className='underline cursor-pointer'><Link href={"/user"}>Click Here</Link></div></div>
      <div className='flex gap-4 bg-white px-6 py-3 rounded-lg'>For Admin Panel <div className='underline cursor-pointer'><Link href={"/admin"}>Click Here</Link></div></div>
    </div>
  )
}
