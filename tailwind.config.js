const defaultTheme = require('tailwindcss/defaultConfig');
const colors = require('tailwindcss/colors')

/** @type {import('tailwindcss').Config} */

module.exports = {
  content: [
    './src/pages/**/*.{js,ts,jsx,tsx,mdx}',
    './src/components/**/*.{js,ts,jsx,tsx,mdx}',
    './src/app/**/*.{js,ts,jsx,tsx,mdx}',
  ],
  theme: {
    extend: {
      colors: {
        ...defaultTheme.colors,
        primary: "#5e3fc9",
        secondary: "#003566",
        secondaryDark: "#101935",
        userPrimary: "#003049",
        userBg: "#042836",
        userSecondary: "#535a94",
        userDark: "#041f2c",
        accentRed: "#e73667",
        accentOrange: "#ff8802",
        ...colors
      },
      backgroundImage: {
        'gradient-radial': 'radial-gradient(var(--tw-gradient-stops))',
        'gradient-conic':
          'conic-gradient(from 180deg at 50% 50%, var(--tw-gradient-stops))',
      },
      fontFamily: {
        brand: ['fa-brands', 'sans-serif'],
        regular: ['fa-regular', 'sans-serif'],
        solid: ['fa-solid', 'cursive'],
        faSolid: ['var(--font-faSolid)']
      }
    },
  },
  plugins: [
    require('tailwind-scrollbar')
  ],
}
